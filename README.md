# DOM Outliner #
*Chrome Extension*

This is a Chrome Extension that creates a simple outline of all DOM elements on a page with the press of a button.

### How do I get set up? ###

* Download from the Chrome Web Store [here](https://chrome.google.com/webstore/detail/outliner/hmdacajmldpieoadpgloddgkeamdhoka?hl=en-US)